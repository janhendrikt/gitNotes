#What is git?
- 'stupid' content tracker
- Decentralized version control
- Used for Linux kernel
- Used by $\approx 4\,$ mil users on github (all the cool kids are using it)


#Why git?
- Easy synchronisation of data @work, @home
- Keeps project history
- Multiple persons can edit documents at the same time
- Merging of multiple person's docs very easy
- Workflow is up to user: central repo can be public (github) or private (for academic use bitbucket/gitorious)

# Where to store your git repository?

- Github, for open source projects; no private repos
- Bitbucket unlimited private repos, nice to not use too much space
- Gitlab, same as bitbucket
- Locally, perfectly fine to just keep everything in a (shared) folder
- Own server offering git over ssh/https

#Introduce yourself
- It's good practice to tell git who you are
- If not done, git tries to infer your username from your hostname
```bash
git config --global user.name "Jan"
git config --global user.email jan@pei.de
```


#Initialize a project
- To initialize a project, move into the folder and type
```bash
git init
```
- This creates a working tree and adds a `.git` folder in the current directory


#The `.git` folder
- a file HEAD
- a directory `objects` containing all the project's objects
- a directory `refs` containing references to objects
- this also contains `heads` and `tags` the name of branches and tags
- NOTE: there is no meta-data stored outside of the working folder, if you
  delete it, your working tree and the repository are gone

#Objects
- an object is identified by its 160-bit SHA-1 hash.
- The ref to it is the 40-byte hex representation


#Workflow
- Git keeps an index and a working tree.
- The folder you are in is the working tree
- Files added to version control are stored as blobs in the index
- The index is moved to the 'real' version-control tree by a commit

#Small Comment
> The core Git is often called "plumbing", with the prettier user
> interfaces on top of it called "porcelain". You may not want to use the
> plumbing directly very often, but it can be good to know what the
> plumbing does for when the porcelain isn't flushing.


#Add files
- Snapshot is stored in temporary staging area, 'the index'
```bash
git add .
```
- Files *need* to be added to be considered for commit

#Commit

```bash
git commit -am 'commit message'
```
- The -a adds all files changed since last commit to staging area before committing
- Every commit has a parent, the previous state

#Show me the changes
```bash
gitk
git log
git log --decorate --graph --stat --oneline #my preference
git diff <commit1> <commit2> [file]
```

#Branches
- Everything is a commit!
- Branches are just syntactic sugar for commits
- Branches are 'nearly free'. So better make to many of them!

```bash
git branch jan [starting point] #creates branch, does not switch to it
git branch -a   #lists all branches
git checkout jan    #switches to branch jan
$work work
git checkout master
git merge jan
git branch -d jan
```


#Collaboration
- Jan clones Christel's project
```bash
jan$ git clone /home/christel/importantproject myrepo
```
- needs to find an existing repo in /importantproject
- creates new folder myrepo

#Collaboration
- Jan changes files, commits them, tells christel she can fetch changes from Jan's master branch
```bash
git-request-pull [-p] <start> <url> [<end>]
```
- Christel pulls changes and tries to merge
```bash
christel$ git pull /home/jan/myrepo master
```


#Merging, integrating changes from others
Pretty easy: 


- First go to target branch, i.e. branch that should be merged into.
```bash
git merge jan
```
- Cautionary remark: never merge with uncommitted changes!
- `git clone` fetches from remote repository and then calls `git merge`

#Conflict resolution
If a conflict arises git will save it in the current files in the following
form:

    Here are lines that are either unchanged from the common
    ancestor, or cleanly resolved because only one side changed.
    <<<<<<< yours:sample.txt
    Conflict resolution is hard;
    let's go shopping.
    =======
    Git makes conflict resolution easy.
    >>>>>>> theirs:sample.txt
    And here is another line that is cleanly resolved or unmodified.

- typically the first is your changes and the latter 'theirs'
- easier is just to use `git mergetool` which will open kdiff3, meld or whatever is currently available for GUI assisted merging

#Conflict resolution
- An alternative style can be used by setting the "merge.conflictstyle"
       configuration variable to "diff3".
- This also includes the original in the diff

#Conflict resolution
- If you just want to integrate changes of other's but keep your version
in conflicting areas
```bash
git merge -s ours <commit>
```
- otherwise `gitmerge -s theirs <commit>`

#Looking into history
```bash
git status
git log
git diff-tree -p HEAD
git show HEAD^4
```
#WIP
- Consider you are working on a rewrite of a whole section but Jan needs a version of the 'latest' document with a minor change
```bash
git checkout christel
$work work work
git stash
git checkout master
$work
git commit -am 'changed some stuff'
git checkout christel
git stash show
git stash apply
```


#Giving names to important milestones

```bash
git tag v2.5 <commit hash>
```
#Common workflows
1. The maintainer, e.g. Jan, creates a repository, adds files, makes the initial commit to a `master` branch
2. The coauthors `git clone /home/troja/gitrepo myrepo`
3. Everyone creates a local branch with `git branch <name>` followed by `git checkout <name>
4. Whenever they are done, they checkout their master branch merge their branch
5. (If they are really nice, they `git pull` from the master repository and do a `git rebase`. This helps if the master branch changed in the meantime, e.g. by other people. This can, of course, be left to the maintainer.)
6. Tell the maintainer they can pull their master repo


#Getting help
- `git help <command>` usually very good
- `man gittutorial` 
- `man gitworkflows`

#Commit as email
git-format-patch
```bash
git format-patch -M upstream..mywork
```

#Merge from email
```bash
git am -3 -i -s ./patch
```

#Administrating repo

```bash
echo 'git   9418/tcp' >> /etc/services
git-daemon
git-shell
git-http-backend
gitweb
```

#Setting up a public repo at sv-bio-02

```bash
```

#Cherry picking


```bash
```
```bash
```
```bash
```
